#include "sensor.h"
#include "mqtt_client.h"

Device awtrix;
#define NUM_SENSORS 4
Sensor sensors[NUM_SENSORS];

MqttClient mqtt;

void publish(Sensor* sens)
{
	char payload[SENSOR_PAYLOAD_LEN];
	int str_ptr = 0;
	sens->json(sens, payload, &str_ptr);
	mqtt_publish(&mqtt, sens->get_topic(sens), payload);
	//printf("T:[%s]\n%s\n", sens->get_topic(sens), payload);
}

int main(void)
{
	mqtt_init(&mqtt, "hass-autodiscovery");
	mqtt_connect(&mqtt, "192.168.224.8", 1883);

	device_init(&awtrix, "Awtrix 7bf108", "awtrix_7bf108", "DJ-Bauer", "0.69");

	int i=0;

	sensor_init_button(&sensors[i++], &awtrix, "Next App", "next", 
			"awtrix_7bf108/nextapp");
	sensor_init_button(&sensors[i++], &awtrix, "Previous App", "prev", 
			"awtrix_7bf108/previousapp");
	sensor_init_button(&sensors[i++], &awtrix, "Dismiss notification", "dismiss",
			"awtrix_7bf108/dismiss");

	sensor_init_switch(&sensors[i++], &awtrix, "Enable Display", "power", 
			"awtrix_7bf108/power", "{\\\"power\\\": true}", "{\\\"power\\\": false}");

	for(int i=0; i<NUM_SENSORS; i++) {
		publish(&sensors[i]);
	}

	device_deinit(&awtrix);

	mqtt_disconnect(&mqtt);
	mqtt_deinit(&mqtt);

	return 0;
}
