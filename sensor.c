#include "sensor.h"

const char* sensor_str[]  = {"button", "sensor", "switch"};

void device_init(Device* dev, const char* name, const char* id, const char* manufacturer, const char* sw_version)
{
	dev->json = device_json;
	dev->name = name;
	dev->id = id;
	dev->manufacturer = manufacturer;
	dev->sw_version = sw_version;
}

void device_deinit(Device* dev)
{
}

void device_json(Device* dev, char buf[128], int* str_ptr, int num_tabs)
{
	static const char* tabs[] = {"", "\t", "\t\t", "\t\t\t", "\t\t\t\t", "\t\t\t\t\t", "\t\t\t\t\t\t", "\t\t\t\t\t\t\t"};
	const char* tab = tabs[(num_tabs+1) % 8]; // This way we can easily implement undefined behavior
	const char* outer_tab = tabs[num_tabs % 8];

	*str_ptr += sprintf(&buf[*str_ptr], "{\n\
%s\"name\": \"%s\",\n\
%s\"id\": \"%s\",\n\
%s\"mf\": \"%s\",\n\
%s\"sw\": \"%s\"\n\
%s}", tab, dev->name, 
    tab, dev->id, 
	tab, dev->manufacturer, 
	tab, dev->sw_version,
	outer_tab);
}

void sensor_init(Sensor* sensor, Device* dev)
{
	sensor->name = "";
	sensor->id = "";
	sensor->json = sensor_json;
	sensor->dev = dev;
	sensor->make_temperature = sensor_make_temperature;
	sensor->make_button = sensor_make_button;
	sensor->make_switch = sensor_make_switch;
	sensor->set_name = sensor_set_name;
	sensor->set_id = sensor_set_id;
	sensor->get_topic = sensor_get_topic;

	sensor_recalc_topic(sensor);
}
void sensor_deinit(Sensor* sensor)
{
}
void sensor_json(Sensor* sensor, char buf[SENSOR_PAYLOAD_LEN], int* str_ptr)
{
	*str_ptr += sprintf(&buf[*str_ptr], "{\n \
	\"name\":\"%s\",\n \
	\"dev\":", sensor->name);

	sensor->dev->json(sensor->dev, buf, str_ptr, 1);
	if(sensor->type == Button || sensor->type == Switch)
		*str_ptr += sprintf(&buf[*str_ptr], ",\n\t\"cmd_t\": \"%s\"", sensor->cmd_t);
	if(sensor->type == Switch) {
		*str_ptr += sprintf(&buf[*str_ptr], ",\n\t\"payload_on\": \"%s\",\n\t\"payload_off\": \"%s\"", sensor->on_payload, sensor->off_payload);
	}
	if(sensor->type == Temperature) {
		*str_ptr += sprintf(&buf[*str_ptr], ",\n\t\"device_class\": \"temperature\"\n\t\"state_class\": \"measurement\",\n\t\"unit_of_measurement\": \"°C\"");
	}
	*str_ptr += sprintf(&buf[*str_ptr], "\n}");
}

void sensor_set_type(Sensor* sensor, SensorType type)
{
	sensor->type = type;
	sensor_recalc_topic(sensor);
}
void sensor_set_name(Sensor* sensor, const char* name)
{
	sensor->name = name;
}
void sensor_set_id(Sensor* sensor, const char* id)
{
	sensor->id = id;
	sensor_recalc_topic(sensor);
}

const char* sensor_get_topic(const Sensor* sensor)
{
	return sensor->topic;
}
void sensor_recalc_topic(Sensor* sensor)
{
	snprintf(sensor->topic, SENSOR_TOPIC_LEN, "homeassistant/%s/%s/%s/config", 
		sensor_str[sensor->type], sensor->dev->id, sensor->id);
}

void sensor_make_temperature(Sensor* sensor)
{
	sensor_set_type(sensor, Temperature);
}
void sensor_make_button(Sensor* sensor, const char* cmd_t)
{
	sensor_set_type(sensor, Button);
	sensor->cmd_t = cmd_t;
}
void sensor_make_switch(Sensor* sensor, const char* cmd_t, const char* on_payload, const char* off_payload)
{
	sensor_set_type(sensor, Switch);
	sensor->cmd_t = cmd_t;
	sensor->on_payload = on_payload;
	sensor->off_payload = off_payload;
}

void sensor_init_temp(Sensor* sensor, Device* dev, const char* name, const char* id)
{
	sensor_init(sensor, dev);
	sensor_set_name(sensor, name);
	sensor_set_id(sensor, id);
	sensor_make_temperature(sensor);
}
void sensor_init_button(Sensor* sensor, Device* dev, const char* name, const char* id, const char* cmd_t)
{
	sensor_init(sensor, dev);
	sensor_set_name(sensor, name);
	sensor_set_id(sensor, id);
	sensor_make_button(sensor, cmd_t);
}
void sensor_init_switch(Sensor* sensor, Device* dev, const char* name, const char* id, const char* cmd_t, const char* payload_on, const char* payload_off)
{
	sensor_init(sensor, dev);
	sensor_set_name(sensor, name);
	sensor_set_id(sensor, id);
	sensor_make_switch(sensor, cmd_t, payload_on, payload_off);
}
