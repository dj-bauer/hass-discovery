#ifndef HASS_DISC_SENSOR_H
#define HASS_DISC_SENSOR_H

#include <stdio.h>

typedef struct s_device {
	const char* name;
	const char* id;
	const char* manufacturer;
	const char* sw_version;
	void (*json)(struct s_device* dev, char buf[128], int* str_ptr, int num_tabs);
} Device;
void device_init(Device* dev, const char* name, const char* id, const char* manufacturer, const char* sw_version);
void device_deinit(Device* dev);
void device_json(Device* dev, char buf[128], int* str_ptr, int num_tabs);

typedef enum e_sensortype {Button, Temperature, Switch} SensorType;
extern const char* sensor_str[];

#define SENSOR_TOPIC_LEN 256
#define SENSOR_PAYLOAD_LEN 1024
typedef struct s_sensor {
	Device* dev;
	SensorType type;
	const char* name;
	const char* id;
	char topic[SENSOR_TOPIC_LEN];

	const char* cmd_t;
	const char* on_payload;
	const char* off_payload;

	void (*json)(struct s_sensor* sensor, char buf[SENSOR_PAYLOAD_LEN], int* str_ptr);
	void (*set_name)(struct s_sensor* sensor, const char* name);
	void (*set_id)(struct s_sensor* sensor, const char* id);
	void (*make_temperature)(struct s_sensor* sensor);
	void (*make_button)(struct s_sensor* sensor, const char* cmd_t);
	void (*make_switch)(struct s_sensor* sensor, const char* cmd_t, const char* on_payload, const char* off_payload);
	const char* (*get_topic)(const struct s_sensor* sensor);
} Sensor;

void sensor_init_temp(Sensor* sensor, Device* dev, const char* name, const char* id);
void sensor_init_button(Sensor* sensor, Device* dev, const char* name, const char* id, const char* cmd_t);
void sensor_init_switch(Sensor* sensor, Device* dev, const char* name, const char* id, const char* cmd_t, const char* payload_on, const char* payload_off);

void sensor_init(Sensor* sensor, Device* dev);
void sensor_deinit(Sensor* sensor);
void sensor_json(Sensor* sensor, char buf[SENSOR_PAYLOAD_LEN], int* str_ptr);
void sensor_set_type(Sensor* sensor, SensorType type);
void sensor_set_name(Sensor* sensor, const char* name);
void sensor_set_id(Sensor* sensor, const char* id);
void sensor_make_temperature(Sensor* sensor);
void sensor_make_button(Sensor* sensor, const char* cmd_t);
void sensor_make_switch(Sensor* sensor, const char* cmd_t, const char* on_payload, const char* off_payload);
const char* sensor_get_topic(const Sensor* sensor);
void sensor_recalc_topic(Sensor* sensor);

#endif
