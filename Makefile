CFLAGS=-g -Wall -fsanitize=address -fsanitize=undefined -Idjmqtt/common_include
LDFLAGS=-fsanitize=address -fsanitize=undefined -Ldjmqtt/.libs -ldjmqtt

all: main.o sensor.o
	${CC} ${LDFLAGS} $^ -o hass-discovery

%.o: %.c Makefile
	${CC} ${CFLAGS} -c -fpic $< -o $@

main.c: sensor.h
sensor.c: sensor.h

clean:
	rm -f *.o hass-discovery
